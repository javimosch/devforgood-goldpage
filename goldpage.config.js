require('dotenv').config({ silent: true })
module.exports = {
    htmlRender: './render/htmlRender.js',
    domRender: './render/domRender.js',
};